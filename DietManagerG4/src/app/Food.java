/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.Map;

/**
 * ISTE 383.800 SWEN
 *
 * @instructor: Kristina Marasovic
 * @Team: G4 Project part: V1 Project name: DietManager
 */
public abstract class Food {

    //attributes of the class Food
    private String type, name;
    private double calories, fat, carb, protein;

    /**
     * Method to get Type of food
     *
     * @return type
     */
    String getType() {
        return type;
    }

    /**
     * Method to get name of the food
     *
     * @return name
     */
    String getName() {
        return name;
    }

    /**
     * Method to get calories in the food
     *
     * @return calories
     */
    double getCalories() {
        return calories;
    }

    /**
     * Method to get Fat
     *
     * @return fat
     */
    double getFat() {
        return fat;
    }

    /**
     * Method to get carb
     *
     * @return carb
     */
    double getCarb() {
        return carb;
    }

    /**
     * Method to get protein in the food
     *
     * @return protein
     */
    double getProtein() {
        return protein;
    }

    /**
     * Method to get Data
     *
     * @return empty string
     */
    String getData() {
        return "";
    }

    /**
     * Method to get IngredientCount
     *
     * @return 0
     */
    public int getIngredientCount() {
        return 0;
    }

    /**
     * Method to get ingredients
     *
     * @return null
     */
    public Map<Food, Double> getIngredients() {
        return null;
    }
}
