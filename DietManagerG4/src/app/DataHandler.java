/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.Map;

/**
 * ISTE 383.800 SWEN
 *
 * @instructor: Kristina Marasovic
 * @Team: G4 Project part: V1 Project name: DietManager
 */
public class DataHandler {

    //Attributes of the class DataHandler
    protected static int HEIGHT = 768;
    protected static int WIDTH = 1024;
    private Map<String, DailyLog> dailyLogMap;
    private Map<String, Food> foodCollectionMap;
    FileHandler fHandler;

    /**
     * Creates/loads all of the data about the diet manager.
     */
    public DataHandler() {
        fHandler = new LogFileHandler();
        dailyLogMap = fHandler.loadData();

        fHandler = new FoodFileHandler();
        foodCollectionMap = fHandler.loadData();
    }

    /**
     * Method to set DailyLogMap
     *
     * @param dailyLogMap
     */
    public void setDailyLogMap(Map<String, DailyLog> dailyLogMap) {
        this.dailyLogMap = dailyLogMap;
    }

    /**
     * Method to get DailyLogMap
     *
     * @return dailyLogMap
     */
    public Map<String, DailyLog> getDailyLogMap() {
        return dailyLogMap;
    }

    /**
     * Method to set FoodCollectionMap
     *
     * @param foodCollectionMap
     */
    public void setFoodCollectionMap(Map<String, Food> foodCollectionMap) {
        this.foodCollectionMap = foodCollectionMap;
    }

    /**
     * Method to get FoodCollectionMap
     *
     * @return foodCollectionMap
     */
    public Map<String, Food> getFoodCollectionMap() {
        return foodCollectionMap;
    }
}
