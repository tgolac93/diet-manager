/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ISTE 383.800 SWEN
 *
 * @instructor: Kristina Marasovic
 * @Team: G4 Project part: V1 Project name: DietManager
 */
public class LogFileHandler extends FileHandler {

    /**
     * Method to save the log.
     *
     * @param date
     * @param dailyLog
     */
    public void save(String date, DailyLog dailyLog) {
        try {
            //Creating new FileWriter
            FileWriter writer = new FileWriter("src/data/foods.csv", true);
            writer.append("\n" + date + "w," + dailyLog.getWeight());
            writer.append("\n" + date + "c," + dailyLog.getCalGoal());

            List<String> foods = dailyLog.getFoodCollection();
            for (String food : foods) {
                writer.append("\n" + date + ",f," + food + "," + dailyLog.getFoodAmount(food));
            }
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Method to load data
     *
     * @return dailyLogMap
     */
    @Override
    public Map<String, DailyLog> loadData() {
        //Creating new HashMap named dailyLogMap
        Map<String, DailyLog> dailyLogMap = new HashMap();
        try {
            List<List<String>> fileData = super.getFileData("log.csv");
            for (List<String> currList : fileData) {
                DailyLog dailyLog = null;
                String date = currList.get(0) + "," + currList.get(1) + "," + currList.get(2);
                boolean exists = false;
                for (String key : dailyLogMap.keySet()) {
                    if (key.equals(date)) {
                        dailyLog = dailyLogMap.get(key);
                        exists = true;
                    }
                }

                if (!exists) {
                    dailyLog = new DailyLog();
                }

                if (currList.get(3).equals("w")) {
                    dailyLog.setWeight(Double.parseDouble(currList.get(4)));
                } else if (currList.get(3).equals("c")) {
                    dailyLog.setCalGoal(Double.parseDouble(currList.get(4)));
                } else if (currList.get(3).equals("f")) {
                    double amount = dailyLog.getFoodAmount(currList.get(4));
                    dailyLog.addFood(currList.get(4), (Double.parseDouble(currList.get(5)) + amount));
                }
                dailyLogMap.put(date, dailyLog);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dailyLogMap;
    }
}
