/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

/**
 * ISTE 383.800 SWEN
 *
 * @instructor: Kristina Marasovic
 * @Team: G4 Project part: V1 Project name: DietManager
 */
public class BasicFood extends Food {

    //Attributes of the class BasicFood
    private String type, name;
    private double calories, fat, carb, protein;
    private final static String TYPE = "b";

    /**
     * Constructor with parameter Initialising attributes
     *
     * @param name
     * @param calories
     * @param fat
     * @param carb
     * @param protein
     */
    public BasicFood(String name, double calories, double fat, double carb, double protein) {
        super();
        this.type = TYPE;
        this.name = name;
        this.calories = calories;
        this.fat = fat;
        this.carb = carb;
        this.protein = protein;
    }

    /**
     * method to get Carb
     *
     * @return carb
     */
    @Override
    public double getCarb() {
        return carb;
    }

    /**
     * Method to get Type of Basic Food
     *
     * @return TYPE
     */
    @Override
    public String getType() {
        return TYPE;
    }

    /**
     * Method to get Name of Basic Food
     *
     * @return name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Method to get Calories in Basic Food
     *
     * @return calories
     */
    @Override
    public double getCalories() {
        return calories;
    }

    /**
     * Method to get Fat in Basic Food
     *
     * @return fat
     */
    @Override
    public double getFat() {
        return fat;
    }

    /**
     * Method to get Protein in Basic Food
     *
     * @return protein
     */
    @Override
    public double getProtein() {
        return protein;
    }

    /**
     * Method to get Data from type, name, calories, fat, carb and protein
     *
     * @return type, name, calories, fat, carb and protein
     */
    @Override
    public String getData() {
        return type + "," + name + "," + calories + "," + fat + "," + carb + "," + protein;
    }

}
