/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.Map;

/**
 * ISTE 383.800 SWEN
 *
 * @instructor: Kristina Marasovic
 * @Team: G4 Project part: V1 Project name: DietManager
 */
public class Recipe extends Food {

    //attributes of the class Recipe
    private final String name;
    private final Map<Food, Double> ingredients;
    private static final String TYPE = "r";

    /**
     * Recipe constructor
     *
     * @param name
     * @param ingredients Initializing attributes
     */
    public Recipe(String name, Map<Food, Double> ingredients) {
        super();
        this.name = name;
        this.ingredients = ingredients;
    }

    /**
     * Method to get name of the recipe
     *
     * @return
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Method to get count of ingredients
     *
     * @return
     */
    @Override
    public int getIngredientCount() {
        return ingredients.size();
    }

    /**
     * Method to format ingredients
     *
     * @return formattedIngredients
     */
    private String formatIngredientString() {
        String formattedIngredients = "";
        for (Food food : ingredients.keySet()) {
            formattedIngredients += "," + food.getName() + "," + ingredients.get(food);
        }

        return formattedIngredients;
    }
    
    /**
     * Method to get Ingredients
     *
     * @return ingredients
     */
    @Override
    public Map<Food, Double> getIngredients() {
        return ingredients;
    }

    /**
     * Method to get Data (TYPE, name and to format ingredients)
     *
     * @return data
     */
    @Override
    public String getData() {
        String data = TYPE + "," + name + formatIngredientString();
        return data;
    }
    
    public String getType(){
        return TYPE;
    }
}
