/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Toggle;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

/**
 * ISTE 383.800 SWEN
 *
 * @instructor: Kristina Marasovic
 * @Team: G4 Project part: V1 Project name: DietManager
 */
public class DietManagerController implements Initializable {

    //Attributes of the class DietManagerController
    private DietManagerUI view;
    public static String DAILY_LOG_DATA = "DailyLogData";
    public static String FOOD_DATA = "FoodData";
    // The list for the ComboBox dropdown menu.
    ObservableList<String> igredient1Options = FXCollections.observableArrayList("Apple", "Biscuit", "Kool Aid");

    /**
     * Default Constructor DietManagerController
     */
    public DietManagerController() {
    }

    /**
     * Method to get Daily Log Data
     *
     * @return DAILY_LOG_DATA
     */
    public static String getDAILY_LOG_DATA() {
        return DAILY_LOG_DATA;
    }

    /**
     * Method to get Food Data
     *
     * @return FOOD_DATA
     */
    public static String getFOOD_DATA() {
        return FOOD_DATA;
    }

    /**
     * Method to display search results
     */
    public void displaySearchResults() {
        //todo
    }

    /**
     * Method to display an error message in case of faulty input.
     *
     * @param type
     * @param message
     */
    private void showErrorMessage(String type, String message) {
        String result = "";
        switch (type) {
            case "d":
                result = "Daily Log ";
                break;
            case "f":
                result = "Add Food ";
                break;
            case "r":
                result = "Add Recipe ";
        }
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(result + "Error");
        //alert.setHeaderText("Results:");
        alert.setContentText(message);

        alert.showAndWait();
    }
    //view food
    @FXML
    private ComboBox foodViewChoice;
    @FXML
    private TextArea foodViewInfo;

    //daily log
    @FXML
    private ToggleGroup logType;
    @FXML
    private TextField logTypeTextValue;
    @FXML
    private DatePicker dailyLogDate;
    @FXML
    private TextField logTypeAmount;

    //add food
    @FXML
    private TextField addFoodName;
    @FXML
    private TextField addFoodCalories;
    @FXML
    private TextField addFoodFat;
    @FXML
    private TextField addFoodCarb;
    @FXML
    private TextField addFoodProtein;
    //@FXML
    //private Button igredientsResetBtn;

    @FXML
    private ComboBox dailyLogFoodChoice;
    @FXML
    private ComboBox logViewChoice;

    //add recipe
    @FXML
    private TextField recipeName;
    @FXML
    private ComboBox igredientChoice1;
    @FXML
    private ComboBox igredientChoice2;
    @FXML
    private ComboBox igredientChoice3;
    @FXML
    private ComboBox igredientChoice4;
    @FXML
    private ComboBox igredientChoice5;
    @FXML
    private ComboBox igredientChoice6;
    @FXML
    private ComboBox igredientChoice7;
    @FXML
    private TextField ingredient1Amount;
    @FXML
    private TextField ingredient2Amount;
    @FXML
    private TextField ingredient3Amount;
    @FXML
    private TextField ingredient4Amount;
    @FXML
    private TextField ingredient5Amount;
    @FXML
    private TextField ingredient6Amount;
    @FXML
    private TextField ingredient7Amount;

    @FXML
    private Label logViewWeight;
    @FXML
    private Label logViewCalories;
    @FXML
    private TextArea logViewFood;

    //private ComboBox[] ingredients = {igredientChoice1,igredientChoice2,igredientChoice3,igredientChoice4,igredientChoice5,igredientChoice6,igredientChoice7};
    /**
     * Method to submit DailyLog
     */
    @FXML
    private void submitDailyLog() {
        String result = "";
        if (dailyLogDate.getValue() == null) {
            this.showErrorMessage("d", "Please Input a Date");
        } else {
            LocalDate ld = dailyLogDate.getValue();
            Calendar c = Calendar.getInstance();
            c.set(ld.getYear(), ld.getMonthValue() - 1, ld.getDayOfMonth());
            result = ld.getYear() + "," + String.format("%02d", (ld.getMonthValue())) + "," + String.format("%02d", ld.getDayOfMonth()) + ",";
            if (logType.getSelectedToggle() == null) {
                this.showErrorMessage("d", "You have to select a log type");
            } else {
                RadioButton selectedRadio = (RadioButton) logType.getSelectedToggle();
                result += selectedRadio.getId() + ",";
                if (selectedRadio.getId().equals("f")) {
                    if (this.dailyLogFoodChoice.getValue().equals("Select Food")) {
                        this.showErrorMessage("d", "Please select a food item");
                    } else {
                        result += dailyLogFoodChoice.getValue() + ",";
                        if (logTypeAmount.getText().equals("")) {
                            this.showErrorMessage("d", "You have to input an amount for food");
                        } else {
                            try {
                                double value = Double.parseDouble(logTypeAmount.getText()) * 1.0;
                                result += value;
                            } catch (NumberFormatException e) {
                                this.showErrorMessage("d", "For amount, you must input a numeric value.");
                            }
                        }
                    }
                } else {
                    try {
                        double value = Double.parseDouble(logTypeTextValue.getText()) * 1.0;
                        result += value;
                    } catch (NumberFormatException e) {
                        this.showErrorMessage("d", "For weight and calories, you must input a numeric value.");
                    }

                }
                //result += logType.getSelectedToggle().
                System.out.println(result);
            }
        }
    }

    /**
     * Method to populate Log view Dropdown
     *
     * @param box Creating new DataHandler
     */
    private void populateLogViewDropDown(ComboBox box) {
        DataHandler handler = new DataHandler();
        Map<String, DailyLog> logMap = handler.getDailyLogMap();

        box.getItems().add("Select Date");
        for (String date : logMap.keySet()) {
            box.getItems().add(date);
        }
        box.setValue("Select Date");
    }

    /**
     * Method to populate FoodDropdown Creating new DataHanlder
     *
     * @param box
     */
    public void populateFoodDropdown(ComboBox box) {
        DataHandler handler = new DataHandler();
        Map<String, Food> foodMap = handler.getFoodCollectionMap();

        box.getItems().add("Select Food");
        for (String name : foodMap.keySet()) {
            box.getItems().add(name);
        }
        box.setValue("Select Food");

    }

    /**
     * Method to get IngredientArray
     *
     * @param box
     * @param text
     * @return result
     */
    private String[] getIngredientArray(ComboBox box, TextField text) {
        String[] result = null;
        if (!box.getValue().equals("Select Food") && !text.getText().equals("")) {
            result = new String[2];
            result[0] = "" + box.getValue();
            result[1] = "" + text.getText();
        }
        return result;
    }

    /**
     * Method to get the ingredient from a 2D array. TODO: Make it not a 2d
     * array as suggested by the professor. Also DRY principle
     *
     * @return result
     */
    private String[][] getIngredient2dArray() {
        String[][] result;
        String[] curIngredient;
        List<String[]> ingredients = new ArrayList<>();
        if (getIngredientArray(this.igredientChoice1, this.ingredient1Amount) != null) {
            curIngredient = getIngredientArray(this.igredientChoice1, this.ingredient1Amount);
            ingredients.add(curIngredient);

        }
        if (getIngredientArray(this.igredientChoice2, this.ingredient2Amount) != null) {
            curIngredient = getIngredientArray(this.igredientChoice2, this.ingredient1Amount);
            ingredients.add(curIngredient);
        }
        if (getIngredientArray(this.igredientChoice3, this.ingredient3Amount) != null) {
            curIngredient = getIngredientArray(this.igredientChoice3, this.ingredient3Amount);
            ingredients.add(curIngredient);
        }
        if (getIngredientArray(this.igredientChoice4, this.ingredient4Amount) != null) {
            curIngredient = getIngredientArray(this.igredientChoice4, this.ingredient4Amount);
            ingredients.add(curIngredient);
        }
        if (getIngredientArray(this.igredientChoice5, this.ingredient5Amount) != null) {
            curIngredient = getIngredientArray(this.igredientChoice5, this.ingredient5Amount);
            ingredients.add(curIngredient);
        }
        if (getIngredientArray(this.igredientChoice6, this.ingredient6Amount) != null) {
            curIngredient = getIngredientArray(this.igredientChoice6, this.ingredient6Amount);
            ingredients.add(curIngredient);
        }
        if (getIngredientArray(this.igredientChoice6, this.ingredient6Amount) != null) {
            curIngredient = getIngredientArray(this.igredientChoice6, this.ingredient6Amount);
            ingredients.add(curIngredient);
        }
        result = new String[ingredients.size()][];
        for (int i = 0; i < ingredients.size(); i++) {

            result[i] = ingredients.get(i);
        }

        return result;

    }

    /**
     * Method to get IngredientMap Creating new DataHandler Creating new
     * LinkedHashMap named ingredients
     *
     * @return ingredients
     */
    private Map getIngredientMap() {
        DataHandler handler = new DataHandler();
        Map<Food, Double> ingredients = new LinkedHashMap();
        Map<String, Food> foodMap = handler.getFoodCollectionMap();
        if (getIngredientArray(this.igredientChoice1, this.ingredient1Amount) != null) {
            ingredients.put(foodMap.get(this.igredientChoice1.getValue()), Double.parseDouble(this.ingredient1Amount.getText()));
        }
        if (getIngredientArray(this.igredientChoice2, this.ingredient2Amount) != null) {
            ingredients.put(foodMap.get(this.igredientChoice2.getValue()), Double.parseDouble(this.ingredient2Amount.getText()));
        }
        if (getIngredientArray(this.igredientChoice3, this.ingredient3Amount) != null) {
            ingredients.put(foodMap.get(this.igredientChoice3.getValue()), Double.parseDouble(this.ingredient3Amount.getText()));
        }
        if (getIngredientArray(this.igredientChoice4, this.ingredient4Amount) != null) {
            ingredients.put(foodMap.get(this.igredientChoice4.getValue()), Double.parseDouble(this.ingredient4Amount.getText()));
        }
        if (getIngredientArray(this.igredientChoice5, this.ingredient5Amount) != null) {
            ingredients.put(foodMap.get(this.igredientChoice5.getValue()), Double.parseDouble(this.ingredient5Amount.getText()));
        }
        if (getIngredientArray(this.igredientChoice6, this.ingredient6Amount) != null) {
            ingredients.put(foodMap.get(this.igredientChoice6.getValue()), Double.parseDouble(this.ingredient6Amount.getText()));
        }
        if (getIngredientArray(this.igredientChoice7, this.ingredient7Amount) != null) {
            ingredients.put(foodMap.get(this.igredientChoice7.getValue()), Double.parseDouble(this.ingredient7Amount.getText()));
        }

        //Creating ArrayList of string named foodNameList
        ArrayList<String> foodNameList = new ArrayList<>();
        int duplicateFoodIndex = -1;
        for (Food food : ingredients.keySet()) {
            duplicateFoodIndex = foodNameList.indexOf(food.getName());
            if (duplicateFoodIndex != -1) {
                this.showErrorMessage("f", "Please select only one food per option.");
            } else {
                foodNameList.add(food.getName());
            }
        }
        return ingredients;
    }

    @FXML
    /**
     * Assigned to submit button in the Add Food tab. For now, it System.out's
     * the result, but later we will save the values properly.
     */
    private void submitAddFood() {
        if (addFoodName.getText().equals("") || addFoodCalories.getText().equals("") || addFoodFat.getText().equals("") || addFoodCarb.getText().equals("") || addFoodProtein.getText().equals("")) {
            this.showErrorMessage("f", "You must fill all form fields.");
        } else {
            try {
                //TODO: Actualy save the result somewhere.
                String name = addFoodName.getText();
                double calories = Double.parseDouble(addFoodCalories.getText()) * 1.0;
                double fat = Double.parseDouble(addFoodFat.getText()) * 1.0;
                double carb = Double.parseDouble(addFoodCarb.getText()) * 1.0;
                double protein = Double.parseDouble(addFoodProtein.getText()) * 1.0;

                BasicFood newFood = new BasicFood(name, calories, fat, carb, protein);
                System.out.println(newFood.getData());
                FoodFileHandler handler = new FoodFileHandler();
                handler.save(newFood);
                this.populateAllFoodDropDowns();
                resetAddFood();
            } catch (NumberFormatException e) {
                this.showErrorMessage("f", "Calories, Fat, Carb and Protein fields must be a numeric value!");
                resetAddFood();
            }
        }
    }

    /**
     * Method to take the given values and saves to the CSV file.
     */
    @FXML
    private void submitAddRecipe() {
        if (recipeName.getText().equals("")) {
            this.showErrorMessage("f", "You must give your recipe a name.");
        } else {
            try {
                String name = recipeName.getText();
                System.out.println(name);
                Map<Food, Double> ingredients = this.getIngredientMap();
                if (ingredients.size() > 1) {
                    Recipe newRecipe = new Recipe(name, ingredients);
                    FoodFileHandler handler = new FoodFileHandler();
                    handler.save(newRecipe);
                    this.populateAllFoodDropDowns();
                    resetAddRecipe();
                } else {
                    this.showErrorMessage("r", "You must have more than 1 ingredient to make a recipe");
                    resetAddRecipe();
                }
            } catch (NumberFormatException e) {
                this.showErrorMessage("r", "Amounts must be a numeric value!");
                resetAddRecipe();
            }
        }
    }
    
    @FXML
    private void deleteDailyLog(){
        this.showErrorMessage("f", "IMPLEMENT ME IF YOU WANT MORE COMMITS!!!");
    }
    
    @FXML
    private void deleteFood(){
        this.showErrorMessage("f", "IMPLEMENT ME IF YOU WANT MORE COMMITS!!!");
    }

    /**
     * Method to reset AddFood
     */
    @FXML
    private void resetAddFood() {
        addFoodName.clear();
        addFoodCalories.clear();
        addFoodFat.clear();
        addFoodCarb.clear();
        addFoodProtein.clear();
    }

    /**
     * Reset function for the addRecipie tab. Not sure if the reset works as
     * planned but if it doesn't please look at
     * https://stackoverflow.com/questions/12142518/combobox-clearing-value-issue
     */
    @FXML
    private void resetAddRecipe() {
        recipeName.clear();
        this.ingredient1Amount.clear();
        this.ingredient2Amount.clear();
        this.ingredient3Amount.clear();
        this.ingredient4Amount.clear();
        this.ingredient5Amount.clear();
        this.ingredient6Amount.clear();
        this.ingredient7Amount.clear();
        igredientChoice1.valueProperty().set("Select Food");
        igredientChoice2.valueProperty().set("Select Food");
        igredientChoice3.valueProperty().set("Select Food");
        igredientChoice4.valueProperty().set("Select Food");
        igredientChoice5.valueProperty().set("Select Food");
        igredientChoice6.valueProperty().set("Select Food");
        igredientChoice7.valueProperty().set("Select Food");

    }

    @FXML
    /**
     * Resets the values in the dailyLog tab.
     */
    private void resetDailyLog() {
        logTypeTextValue.clear();
        logTypeTextValue.setPromptText("");
        logTypeAmount.clear();
        logTypeAmount.setPromptText("");
        this.dailyLogFoodChoice.valueProperty().set("Select Food");
        logTypeTextValue.setEditable(false);
        dailyLogDate.setValue(null);
        if (logType.getSelectedToggle() != null) {
            logType.getSelectedToggle().setSelected(false);
        }

        //logTypeTextValue.setPromptText("");
    }

    /**
     * Method to populate all food dropdowns
     */
    public void populateAllFoodDropDowns() {
        this.populateFoodDropdown(dailyLogFoodChoice);
        //this.populateFoodDropdown(foodViewChoice);
        this.populateFoodDropdown(igredientChoice1);
        this.populateFoodDropdown(igredientChoice2);
        this.populateFoodDropdown(igredientChoice3);
        this.populateFoodDropdown(igredientChoice4);
        this.populateFoodDropdown(igredientChoice5);
        this.populateFoodDropdown(igredientChoice6);
        this.populateFoodDropdown(igredientChoice7);
        this.populateFoodDropdown(foodViewChoice);
    }

    /**
     * Method to initialize
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.populateLogViewDropDown(logViewChoice);
        // The ComboBox values have to be added manually here. The list for the
        this.populateAllFoodDropDowns();
        // setItems() can be found on line 41.
        //igredientChoice1.setValue("Please Select An Ingredient");
        //igredientChoice1.setItems(igredient1Options);
        logType.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if (logType.getSelectedToggle() != null) {
                    RadioButton selectedRadio = (RadioButton) logType.getSelectedToggle();
                    System.out.println("Selected: " + selectedRadio.getText());
                    logTypeTextValue.setEditable(true);
                    logTypeTextValue.clear();
                    if (!selectedRadio.getId().equals("f")) {
                        logTypeTextValue.setPromptText("Input " + selectedRadio.getText());
                    }

                    System.out.println(selectedRadio.getId());
                    //logTypeTextValue.setText("Selected: " + logType.getSelectedToggle().getUserData().toString());
                    if (selectedRadio.getId().equals("f")) {
                        logTypeTextValue.setPromptText("weight and calories only");
                        dailyLogFoodChoice.setDisable(false);
                        logTypeAmount.setEditable(true);
                        logTypeAmount.setPromptText("Input Amount");
                    } else {
                        dailyLogFoodChoice.setDisable(true);
                        logTypeAmount.setEditable(false);
                        logTypeAmount.clear();
                        logTypeAmount.setPromptText("Only for food");
                    }
                }
            }

        });

        this.foodViewChoice.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue.equals("Select Food")) {
                    foodViewInfo.clear();
                } else {
                    DataHandler handler = new DataHandler();
                    Map<String, Food> foodMap = handler.getFoodCollectionMap();
                    Food curFood = foodMap.get(newValue);

                    String result = "";

                    if (curFood.getType().equals("b")) {
                        result += "Type: Basic Food\n"
                                + "------------------------\n"
                                + "- Calories: " + curFood.getCalories() + "\n"
                                + "- Fat: " + curFood.getFat() + "\n"
                                + "- Carb: " + curFood.getCarb() + "\n"
                                + "- Protein: " + curFood.getProtein();
                    } else if (curFood.getType().equals("r")) {
                        result += "Type: Recipe\n"
                                + "------------------------\n"
                                + "Ingredients:\n";

                        Map<Food, Double> ingredients = curFood.getIngredients();
                        for (Food food : ingredients.keySet()) {
                            result += "- " + food.getName() + " - " + ingredients.get(food) + "\n";
                        }
                        result += "------------------------\n"
                                + "Total calories: IMPLEMENT ME!!!";
                        
                    }

                    foodViewInfo.setText(result);
                }
            }

        });

        this.logViewChoice.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue.equals("Select Date")) {
                    logViewWeight.setText("0");
                    logViewCalories.setText("0");
                    logViewFood.clear();
                } else {
                    //Creating new DataHandler
                    DataHandler handler = new DataHandler();
                    Map<String, DailyLog> logMap = handler.getDailyLogMap();
                    DailyLog curLog = logMap.get(newValue);
                    System.out.println("test " + curLog.getWeight());
                    logViewWeight.setText(curLog.getWeight() + "");
                    logViewCalories.setText(curLog.getCalGoal() + "");
                    List<String> curLogFoods = curLog.getFoodCollection();
                    String resultString = "";
                    for (String curFood : curLogFoods) {
                        resultString += curFood + " - " + curLog.getFoodAmount(curFood) + "\n";
                    }
                    logViewFood.setText(resultString);

                }
            }

        });
    }

}
