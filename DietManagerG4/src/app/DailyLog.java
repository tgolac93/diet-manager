/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ISTE 383.800 SWEN
 *
 * @instructor: Kristina Marasovic
 * @Team: G4 Project part: V1 Project name: DietManager
 */
public class DailyLog {

    //Attributes of the class DailyLog
    private double weight, calGoal;
    private Map<String, Double> dailyFood;

    /**
     * The default constructor Initialising attributes Creating HashMap
     * dailyFood
     */
    public DailyLog() {
        weight = 180.0;
        calGoal = 2000.0;
        dailyFood = new HashMap<>();
    }

    /**
     * The normal constructor which forwards the values.
     *
     * @param weight
     * @param calGoal Creating new HashMap dailyFood
     */
    public DailyLog(double weight, double calGoal) {
        this.weight = weight;
        this.calGoal = calGoal;
        dailyFood = new HashMap<>();
    }

    /**
     * Method to get Weight
     *
     * @return weight
     */
    public double getWeight() {
        return weight;
    }

    /**
     * Method to set Weight
     *
     * @param weight
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * Method to get Cal Goal
     *
     * @return calGoal
     */
    public double getCalGoal() {
        return calGoal;
    }

    /**
     * Method to set Cal Goal
     *
     * @param calGoal
     */
    public void setCalGoal(double calGoal) {
        this.calGoal = calGoal;
    }

    /**
     * Method to get the amount of food for a specific food.
     *
     * @param food
     * @return result
     */
    public double getFoodAmount(String food) {
        double result = 0.0;
        if (dailyFood.containsKey(food)) {
            result = dailyFood.get(food);
        }
        return result;
    }

    /**
     * Method to get a single piece of food based on the input.
     *
     * @param food Creating array of String named results
     * @return results
     */
    public String[] getSingleFood(String food) {
        String[] results = new String[]{};
        if (dailyFood.containsKey(food)) {
            results = new String[]{food, "" + this.getFoodAmount(food)};
        }
        return results;
    }

    /**
     * Method which returns all of the available food. Creating ArrayList of
     * Strings named food
     *
     * @return foods
     */
    public List<String> getFoodCollection() {
        List<String> foods = new ArrayList<>();
        for (String f : dailyFood.keySet()) {
            foods.add(f);
        }
        return foods;
    }

    /**
     * Method to add the food with the quantity based on the input
     *
     * @param food
     * @param foodQuantity
     */
    public void addFood(String food, double foodQuantity) {
        dailyFood.put(food, foodQuantity);
    }
}
