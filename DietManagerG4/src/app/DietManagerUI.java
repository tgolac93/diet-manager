/*
 * This class is the "runner" calss.
 */
package app;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * ISTE 383.800 SWEN
 *
 * @instructor: Kristina Marasovic
 * @Team: G4 Project part: V1 Project name: DietManager
 */
public class DietManagerUI extends Application {

    /**
     * Default constructor DietManagerUI
     */
    public DietManagerUI() {
        //Creating new DataHandler
        DataHandler model = new DataHandler();
        DietManagerController cont = new DietManagerController();
    }

    //main method
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Method to start DietManager
     *
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));

        Scene scene = new Scene(root);

        stage.setTitle("Diet Manager G4");
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);
    }
}
