package app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * ISTE 383.800 SWEN
 *
 * @instructor: Kristina Marasovic
 * @Team: G4 Project part: V1 Project name: DietManager
 */
public abstract class FileHandler {

    //attribute of the class FileHandler
    private List<List<String>> dataList;

    public abstract Map loadData();

    /**
     * Method to save
     */
    public void save() {
    }

    /**
     * Method to get file data
     *
     * @param fileName
     * @return dataList
     */
    public List<List<String>> getFileData(String fileName) {
        //Creating new ArrayList named dataList
        dataList = new ArrayList<>();
        try {
            //Creating new File
            File file = new File("src/data/" + fileName);
            //Creating new BufferedReader
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            List<String> lineData;
            //Reading line
            String curLine = br.readLine();

            while (curLine != null) {
                lineData = Arrays.asList(curLine.split(","));
                dataList.add(lineData);
                curLine = br.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataList;
    }
}
