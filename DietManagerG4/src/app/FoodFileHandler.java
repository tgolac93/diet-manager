/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * ISTE 383.800 SWEN
 *
 * @instructor: Kristina Marasovic
 * @Team: G4 Project part: V1 Project name: DietManager
 */
public class FoodFileHandler extends FileHandler {

    /**
     * Method to populate the map based on the given letter.Each letter performs
     * a different function.
     *
     * @return foodCollectionMap
     */
    @Override
    public Map<String, Food> loadData() {
        //Creating new HashMap named foodCollectionMap
        Map<String, Food> foodCollectionMap = new HashMap();
        List<List<String>> fileData = super.getFileData("foods.csv");
        Food food = null;
        for (List<String> currList : fileData) {
            String type = currList.get(0);
            String name = "";
            switch (type) {
                case "b":
                    name = currList.get(1);
                    double calories = Double.parseDouble(currList.get(2));
                    double fat = Double.parseDouble(currList.get(3));
                    double carb = Double.parseDouble(currList.get(4));
                    double protein = Double.parseDouble(currList.get(5));

                    food = new BasicFood(name, calories, fat, carb, protein);

                    break;

                case "r":
                    name = currList.get(1);
                    Map<Food, Double> ingredients = new LinkedHashMap();
                    for (int i = 2; i < currList.size(); i += 2) {
                        ingredients.put(foodCollectionMap.get(currList.get(i)), Double.parseDouble(currList.get(i + 1)));
                    }
                    food = new Recipe(name, ingredients);
            }
            foodCollectionMap.put(name, food);
        }
        return foodCollectionMap;
    }

    /**
     * Method to save the information to the collectionMap
     *
     * @param food
     */
    public void save(Food food) {
        try {
            //Creating new FileWriter
            FileWriter writer = new FileWriter("src/data/foods.csv", true);
            writer.append("\n");
            writer.append(food.getData());
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
