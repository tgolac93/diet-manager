package badcomputer;

public class ComputerTester {

    public static void main(String[] args) {

        Computer computer = new Computer();
        Monitor monitor = new Monitor();        
        Projector projector = new Projector();
        
        computer.addDisplay(monitor, "monitor");
        //computer.display();
        
        //What if we have Projector        
        computer.addDisplay(projector, "projector");
       // computer.display();
       
       System.out.println("Demo - Display individual:");
       computer.displaySpecificScreen("monitor");
       
       System.out.println("\nDemo - Display all:");
       computer.displayAll();

    }

}
