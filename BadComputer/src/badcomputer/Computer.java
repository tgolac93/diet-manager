package badcomputer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.swing.JLabel;

/**
 *
 * @author Kristina Marasovic <kristina.marasovic@rit.edu>
 */
public class Computer {

    //atrributes of the Computer class
    private final Map<String, Screen> displayMap;
    //private Screen display;

    public Computer() {
        displayMap = new HashMap<>();
    }

    /**
     * Displays a specific screen
     *
     * @param name
     */
    public void displaySpecificScreen(String name) {
        displayMap.get(name.toLowerCase()).display();
    }

    /**
     * Displays all screens
     */
    public void displayAll() {
        for (String key : displayMap.keySet()) {
            displayMap.get(key).display();
        }
    }

    /**
     * adding display
     *
     * @param d
     * @param name
     */
    public void addDisplay(Screen d, String name) {
        displayMap.put(name.toLowerCase(), d);
    }

}
