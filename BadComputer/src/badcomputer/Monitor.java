
package badcomputer;

/**
 *
 * @author Kristina Marasovic <kristina.marasovic@rit.edu>
 */
class Monitor implements Screen{

    public void display() {
        System.out.println("Display through Monitor");
    }
    
}
