package badcomputer;

/**
 *
 * @author Kristina Marasovic <kristina.marasovic@rit.edu>
 */
public class Projector implements Screen {

    public void display() {
        System.out.println("Display through Projector.");
    }

}
