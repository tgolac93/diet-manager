
import java.util.Scanner;

public class Client {

    public static void main(String[] args) {
        WeatherStation ws = new WeatherStation();
        Thread thread = new Thread(ws);
        Scanner scan = new Scanner(System.in);

        String selectedUI;
        boolean isNotValidUI = true;
        do {
            System.out.print("What UI would you like to use(TextUI/SwingUI)? ");
            selectedUI = scan.nextLine().toLowerCase();
            switch (selectedUI) {
                case "textui":
                    TextUI textUi = new TextUI(ws);
                    isNotValidUI = false;
                    break;
                case "swingui":
                    SwingUI swingUi = new SwingUI(ws);
                    isNotValidUI = false;
                    break;
                default:
                    System.out.println("That kind of UI does not exist.\n");
                    break;
            }
        } while (isNotValidUI);
        thread.start();

    }
}
