


import java.util.Observer;
import java.util.Observable;

/**
 * The TextUI class is an observer of the WeatherStation that, when it receives
 * an update message, prints the temperature in Celsius and Kelvin.
 *
 * The main method for the text based monitoring application is here as well.
 *
 * @author Michael J. Lutz, Kristina Marasovic <kristina.marasovic@rit.edu>
 */
public class TextUI implements Observer {

    private final WeatherStation station;

    /*
     * Remember the station we're attached to and add ourselves as an observer.
     */
    public TextUI(WeatherStation station) {
        this.station = station;
        this.station.addObserver(this);
    }

    /**
     * Called when WeatherStation gets another reading. The Observable should be
     * the station; the Object argument arg is ignored.
     *
     * @param o the observable
     * @param arg custom object containing data passed from the observable (if
     * any)
     */
    @Override
    public void update(Observable o, Object arg) {

        // Check for spurious updates from unrelated objects.
        if (o != station) {
            return;
        }
        System.out.printf(
                "Temperature: %6.2f C %6.2f F %6.2f K%nPressure: %8.2f inches %8.2f mbar%nHumidity: %8.0f percent%n%n",
                station.getReading(MeasureUnit.CELSIUS),
                station.getReading(MeasureUnit.FARENHEIT),
                station.getReading(MeasureUnit.KELVIN),
                station.getReading(MeasureUnit.INHG),
                station.getReading(MeasureUnit.MBAR),
                station.getReading(MeasureUnit.PERCENT)
        );
    }
}
