
import java.util.HashMap;
import java.util.Map;
import edu.rit.marasovic.swen383.thirdparty.sensor.HumidityReader;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Korisnik
 */
public class SensorFactory {

    ISensor sensor;

    public SensorFactory() {

    }

    public ISensor getSensor(String type) {
        switch (type){
            case "Temperature":
                sensor = new TemperatureSensor();
                break;
            case "Pressure":
                sensor = new PressureSensor();
                break;
            case "Humidity":
                sensor = new HumiditySensor();
                break;
            default:
                sensor = null;
        }
        return sensor;
    }

}
