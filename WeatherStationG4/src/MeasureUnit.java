/**
 *
 * @author Bruno
 */
       public enum MeasureUnit {
        KELVIN(1.0,0.0,"Temperature"),
        CELSIUS(1.0,-273.15,"Temperature"),
        FARENHEIT(1.8,-459.67,"Temperature"),
        INHG(1.0,0.0,"Pressure"),
        MBAR(33.86387,0.0,"Pressure"),
        PERCENT(1.0, 0.0, "Humidity");

        private final double param1;
        private final double param2;
        private final String param3;

        MeasureUnit(double param1, double param2, String param3) {
            this.param1 = param1;
            this.param2 = param2;
            this.param3 = param3;
        }

        public double get(double reading) {
            return reading* param1 + param2;
        }
        
        public String getType(){
            return param3;
        }
    }
