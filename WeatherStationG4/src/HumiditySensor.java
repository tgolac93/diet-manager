
import edu.rit.marasovic.swen383.thirdparty.sensor.HumidityReader;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dxt5777
 */
public class HumiditySensor implements ISensor{
    private HumidityReader humidityReader;
    
    public HumiditySensor() {
        humidityReader = new HumidityReader();
    }
    @Override
    public double read() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return (double) humidityReader.get();
    }
}
