

/*
 * Initial Author
 *      Michael J. Lutz
 *
 * Other Contributers
 *
 * Acknowledgements
 */

 /*
 * Swing UI class used for displaying the information from the
 * associated weather station object.
 * This is an extension of JFrame, the outermost container in
 * a Swing application.
 */
import java.awt.Font;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class SwingUI extends JFrame implements Observer {

    private Map<MeasureUnit, JLabel> labelMap;
    private Map<MeasureUnit, String> nameMap;
    
    /*
     * A Font object contains information on the font to be used to
     * render text.
     */
    private static Font labelFont
            = new Font(Font.SERIF, Font.PLAIN, 72);

    private WeatherStation station;

    /*
     * Create and populate the SwingUI JFrame with panels and labels to
     * show the temperatures.
     */
    public SwingUI(WeatherStation station) {
        super("Weather Station");
        this.station = station;
        this.station.addObserver(this);
        labelMap = new HashMap<MeasureUnit, JLabel>();
        nameMap = new HashMap<MeasureUnit,String>();
        nameMap.put(MeasureUnit.KELVIN, " Kelvin ");
        nameMap.put(MeasureUnit.CELSIUS, " Celsius ");
        nameMap.put(MeasureUnit.FARENHEIT, " Farenheit ");
        nameMap.put(MeasureUnit.INHG, " Inches ");
        nameMap.put(MeasureUnit.MBAR, " Millibars ");
        nameMap.put(MeasureUnit.PERCENT, " Percent ");
        /*
         * WeatherStation frame is a grid of 1 row by an indefinite
         * number of columns.
         */
        this.setLayout(new GridLayout(1, 0));

        /*
         * There are two panels, one each for Kelvin and Celsius, added to the
         * frame. Each Panel is a 2 row by 1 column grid, with the temperature
         * name in the first row and the temperature itself in the second row.
         */
        for (MeasureUnit currUnit : MeasureUnit.values()) {
            //String unitName = currUnit.toString();
            //unitName = unitName.substring(0,1) + unitName.substring(1,unitName.length()).toLowerCase();
            createPanel(currUnit, nameMap.get(currUnit));
        }

        /*
         * Set up the frame's default close operation pack its elements,
         * and make the frame visible.
         */
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }

    /*
     * Create a Label with the initial value <title>, place it in
     * the specified <panel>, and return a reference to the Label
     * in case the caller wants to remember it.
     */
    private JLabel createLabel(String title, JPanel panel) {
        JLabel label = new JLabel(title);

        label.setHorizontalAlignment(JLabel.CENTER);
        label.setVerticalAlignment(JLabel.TOP);
        label.setFont(labelFont);
        panel.add(label);

        return label;
    }

    private JPanel createPanel(MeasureUnit unit, String title) {
        JPanel panel = new JPanel(new GridLayout(2, 1));
        this.add(panel);
        createLabel(title, panel);
        labelMap.put(unit, createLabel("", panel));
        return panel;
    }

    private void setLabel(MeasureUnit unit, double value) {
        labelMap.get(unit).setText(String.format("%6.2f", value));
    }

    @Override
    public void update(Observable o, Object arg1) {
        if (o != station) {
            return;
        }

        for (MeasureUnit currUnit : MeasureUnit.values()) {
            setLabel(currUnit, station.getReading(currUnit));
        }
    }
}
