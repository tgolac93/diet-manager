

/*
 * Initial Author
 *      Michael J. Lutz
 *
 * Other Contributers
 *
 * Acknowledgements
 */

 /*
 * Class for a simple computer based weather station that reports the current
 * temperature (in Celsius) every second. The station is attached to a
 * sensor that reports the temperature as a 16-bit number (0 to 65535)
 * representing the Kelvin temperature to the nearest 1/100th of a degree.
 *
 * This class is implements Runnable so that it can be embedded in a Thread
 * which runs the periodic sensing.
 */
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import javax.swing.JLabel;

public class WeatherStation extends Observable implements Runnable {

    //private final TemperatureSensor temperatureSensor; // Temperature sensor.

    //private final PressureSensor pressureSensor;

    private final long PERIOD = 1000;      // 1 sec = 1000 ms.

    private double reading;           // actual sensor reading.

    private Map<MeasureUnit, ISensor> sensorMap;

    private Map<MeasureUnit, Double> readingMap;

    /*
     * When a WeatherStation object is created, it in turn creates the sensor
     * object it will use.
     */
    public WeatherStation() {
        //temperatureSensor = new TemperatureSensor();
        //pressureSensor = new PressureSensor();
        sensorMap = sensorMap = new HashMap<>();
        sensorMap.put(MeasureUnit.KELVIN, new SensorFactory().getSensor("Temperature"));
        sensorMap.put(MeasureUnit.MBAR, new SensorFactory().getSensor("Pressure"));
        sensorMap.put(MeasureUnit.PERCENT,new SensorFactory().getSensor("Humidity"));
        readingMap = new HashMap<>();
        
    }

    /*
     * The "run" method called by the enclosing Thread object when started.
     * Repeatedly sleeps a second, acquires the current temperature from
     * its sensor, and reports this as a formatted output string.
     */
    public void run() {

        while (true) {
            getReadings();
            setChanged();
            notifyObservers();
            try {
                Thread.sleep(PERIOD);
            } catch (InterruptedException e) {
            }
        }
    }

    public double getReading(MeasureUnit unit) {
        return readingMap.get(unit);
    }

    public void getReadings() {
        double temperatureReading = sensorMap.get(MeasureUnit.KELVIN).read();
        double pressureReading = sensorMap.get(MeasureUnit.MBAR).read();
        double humidityReading = sensorMap.get(MeasureUnit.PERCENT).read();
        for (MeasureUnit currUnit : MeasureUnit.values()) {
            if (currUnit.getType().equals("Temperature")) {
                readingMap.put(currUnit, currUnit.get(temperatureReading));
            } else if (currUnit.getType().equals("Pressure")) {
                readingMap.put(currUnit, currUnit.get(pressureReading));
            } else if(currUnit.getType().equals("Humidity")) {
                readingMap.put(currUnit,currUnit.get(humidityReading));
            }
        }
    }
}
